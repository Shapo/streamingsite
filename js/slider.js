function shiftTop() {
    const boxes = document.querySelectorAll(".box");
    const tmpNode = boxes[0];
    boxes[0].className = "box move-out-from-top";

    setTimeout(function() {
        if (boxes.length > 5) {
            tmpNode.classList.add("box--hide");
            boxes[5].className = "box move-to-position5-from-left";
        }
        boxes[1].className = "box move-to-position1-from-left";
        boxes[2].className = "box move-to-position2-from-left";
        boxes[3].className = "box move-to-position3-from-left center-box";
        boxes[4].className = "box move-to-position4-from-left";
        boxes[0].remove();

        document.querySelector(".cards__container").appendChild(tmpNode);

    }, 100);

}

function shiftBottom() {
    const boxes = document.querySelectorAll(".box");
    boxes[4].className = "box move-out-from-bottom";
    setTimeout(function() {
        const noOfCards = boxes.length;
        if (noOfCards > 4) {
            boxes[4].className = "box box--hide";
        }

        const tmpNode = boxes[noOfCards - 1];
        tmpNode.classList.remove("box--hide");
        boxes[noOfCards - 1].remove();
        let parentObj = document.querySelector(".cards__container");
        parentObj.insertBefore(tmpNode, parentObj.firstChild);
        tmpNode.className = "box move-to-position1-from-right";
        boxes[0].className = "box move-to-position2-from-right";
        boxes[1].className = "box move-to-position3-from-right center-box";
        boxes[2].className = "box move-to-position4-from-right";
        boxes[3].className = "box move-to-position5-from-right";
    }, 100);

}

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });
  

// const ul = document.querySelector('.cards__container');

// ul.addEventListener ('click', e => {
//     console.log(e.target);
// });

// window.addEventListener("wheel", e => {
//     const delta = Math.sign(event.deltaY);
//     if(delta > 0) {
//         shiftTop();
//     }
//     console.info(delta);
// });

// media player 

let fScreen = document.querySelector(".center-box video");
let video = document.querySelector('.center-box video')
let progress = document.querySelector('.center-box .progress');
let progressBar = document.querySelector('.center-box .progress__bar');
let toggle = document.querySelector('.center-box button');
let titles = document.querySelector('.center-box .titles__wrapper');


// fullscren request with button class = "fullsc__button"

function openFullscreen() {
  if (fScreen.requestFullscreen) {
    fScreen.requestFullscreen();
  } else if (fScreen.mozRequestFullScreen) { /* Firefox */
    fScreen.mozRequestFullScreen();
  } else if (fScreen.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    fScreen.webkitRequestFullscreen();
  } else if (fScreen.msRequestFullscreen) { /* IE/Edge */
    fScreen.msRequestFullscreen();
  }
}

// progress bar is fully implemented on central card


function handleProgress() {
  const percent = (video.currentTime / video.duration) * 100;
  progressBar.style.width = `${percent}%`;
  console.log(percent)
}

function hideTitlesOnPlay (){
  togglePlay();
  hideTitles();
}

function togglePlay() {
  const method = video.paused ? 'play' : 'pause';
  video[method]();
  
}

function updateButton() {
  const icon = this.paused ? '►' : '❚❚';
  console.log(icon);
  toggle.textContent = icon;
}

function hideTitles() {
  
    titles.classList.add = 'hide__titles';

}

toggle.addEventListener('click', hideTitlesOnPlay);
video.addEventListener('timeupdate', handleProgress);
video.addEventListener('play', updateButton);
video.addEventListener('pause', updateButton);


// Keyboard navigation with arrowkeys 


document.body.addEventListener('keydown', e => {
  switch(e.key){
      case 'ArrowUp':
          shiftTop();
          break;
      case 'ArrowLeft':
          shiftTop();
          break;
      case 'ArrowDown':
          shiftBottom();
          break;
      case 'ArrowRight':
          shiftBottom();
          break;
      }
      
});



  
document.addEventListener('touchstart', handleTouchStart, false);        
document.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;                                                        
var yDown = null;

function getTouches(evt) {
  return evt.touches ||             // browser API
         evt.originalEvent.touches; // jQuery
}                                                     

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];                                      
    xDown = firstTouch.clientX;                                      
    yDown = firstTouch.clientY;                                      
};                                                

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            /* left swipe */ 
        } else {
            /* right swipe */
        }                       
    } else {
        if ( yDiff > 0 ) {
            /* up swipe */ 
            shiftTop();
            
        } else { 
            /* down swipe */
            shiftBottom();
        }                                                                 
    }
    /* reset values */
    xDown = null;
    yDown = null;                                             
};
