document.addEventListener('touchstart', handleTouchStart, false); //bind & fire - evento di inizio tocco
document.addEventListener('touchmove', handleTouchMove, false); // bind & fire - evento di movimento durante il tocco
var xDown = null;
var yDown = null;
function handleTouchStart(evt) {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
};
function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    } //nessun movimento
    var xUp = evt.touches[0].clientX;
    var yUp = evt.touches[0].clientY;
    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;
    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*Trovo quello più significativo sulle assi X e Y*/
        if ( xDiff > 0 ) {

            /* swipe sinistra */
            console.log("Swipe SINISTRA");

 
        } else {
            /* swipe destra */
            console.log("Swipe DESTRA");

        }//right
    } else {
        if ( yDiff > 0 ) {

            /* swipe alto */
            console.log("Swipe ALTO");

        } else {

            /* swipe basso */
            console.log("Swipe BASSO");

        }
    }
    /* reset values */
    xDown = null;
    yDown = null;
};
//Gesture