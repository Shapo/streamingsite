const elem = document.querySelector("li:nth-child(3) .viewer");
const video = document.querySelector('body > div.container > ul > li:nth-child(3) > div.player > video')
const progress = video.querySelector('li:nth-child(3) .progress');
const progressBar = video.querySelector('li:nth-child(3) .progress .progress__bar');
const toggle = video.querySelector('.toggle');


elem.addEventListener('click', () => console.log('Yes!'));

function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

// function handleRangeUpdate() {
//   video[this.name] = this.value;
// }

function handleProgress() {
  const percent = (video.currentTime / video.duration) * 100;
  progressBar.style.width = `${percent}%`;
}

function scrub(e) {
  const scrubTime = (e.offsetX / progress.offsetWidth) * video.duration;
  video.currentTime = scrubTime;
}

// video.addEventListener('timeupdate', handleProgress);

function togglePlay() {
  const method = video.paused ? 'play' : 'pause';
  video[method]();
}

video.addEventListener('click', togglePlay);

